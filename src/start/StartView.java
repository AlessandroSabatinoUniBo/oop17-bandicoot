package start;

import javafx.event.ActionEvent;
import javafx.stage.Stage;

public interface StartView {
	
	void start(Stage primaryStage);
	
	void doButtonStart(ActionEvent event);

}
