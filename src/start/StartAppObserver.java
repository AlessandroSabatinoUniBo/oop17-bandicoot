package start;

import javafx.stage.Stage;

public interface StartAppObserver {

	void start(Stage primaryStage);
	
	void init(Stage primaryStage);
	
}
